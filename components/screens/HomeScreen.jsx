import { View, Text, StyleSheet, Image,FlatList,TouchableOpacity, ActivityIndicator } from 'react-native'
import React, { useEffect, useState } from 'react'
import { COLORS } from '../constants/theme'
import axios from 'axios'



export const HomeCard=({item,navigation})=>{

    return(
        <TouchableOpacity activeOpacity={0.8} onPress={()=>navigation.navigate('Detail',{
            item:item
        })}>
        <View style={{padding:10,flexDirection:'row',gap:4,alignItems:'center',justifyContent:'space-between'}}>
            <View style={{flexDirection:'row',gap:4,alignItems:'center'}}>
            <Image style={{width:60,height:60,borderRadius:10}} source={{uri:item.thumbnailUrl}} />
            <View style={{width:200}}>
            <Text>{item.title}</Text>
            </View>
            </View>
            <TouchableOpacity>
            <Text >❤️</Text>
            </TouchableOpacity>
        </View>
        </TouchableOpacity>
    )
}
const HomeScreen = ({navigation}) => {
    const [posts,setPosts]=useState([]);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    
    const fetchPosts=async()=>{
        setLoading(true);
        const response=await axios.get(`https://jsonplaceholder.typicode.com/photos?_page=${page}&_limit=10`)
        const newData = await response.data;
        setPosts(prevData=>[...prevData,...newData]);
        setLoading(false);
    }
    useEffect(()=>{
        fetchPosts();
    },[page])
    

    const loadMoreHandler=()=>{
        setPage(page+1)
    }

    const renderFooter = () => {
        return loading ? (
          <View style={{ marginVertical: 20 }}>
            <ActivityIndicator animating size="large" />
          </View>
        ) : null;
      };

  return (
    <View style={{backgroundColor:'#ffeef0',flex:1}}>
        <View style={{padding:8}}>
        <View style={{justifyContent:'space-between',alignItems:'center',flexDirection:'row'}}>
            <Text style={{fontSize:32,fontWeight:'bold'}}>{`<`}</Text>
            <Text style={{fontSize:20,fontWeight:'bold'}}>{`...`}</Text>
        </View>
        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
            <View>
            <Text style={{fontSize:20}}>Today</Text>
            <Text style={{fontSize:24,fontWeight:'bold'}}>11 February</Text>
            </View>

            <View>
            <Text style={{fontSize:16}}>Select Activity</Text>
            <Text style={{fontSize:16,fontWeight:'bold'}}>gym</Text>
            </View>
        </View>
        </View>

        <View style={{backgroundColor:'white',flex:1 ,borderTopLeftRadius:28,borderTopRightRadius:28,marginTop:40,padding:4}}> 
        <FlatList
        style={{padding:10}}
        data={posts}
        renderItem={({item})=><HomeCard navigation={navigation} item={item}/>}
        keyExtractor={(item)=>item.id}
        showsVerticalScrollIndicator={false}
        onEndReached={loadMoreHandler}
        onEndReachedThreshold={0.1}
        ListFooterComponent={renderFooter}
        />
        </View>
 
    </View>
  )
}

export default HomeScreen

const styles=StyleSheet.create({
    header:{
        height:60,
        backgroundColor:COLORS.primary,
        justifyContent:'center',
    },
    header_text:{
        color:'white',
        fontWeight:'bold',
        padding:8,
        fontSize:20
    }
})