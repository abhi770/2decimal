import { View, Text, Image,TouchableOpacity,ScrollView } from 'react-native'
import React from 'react'
import bg_01 from '../assets/bg_01.jpg'

const DetailScreen = ({route,navigation}) => {
    const {item}=route.params;
  return (
    <View style={{flex:1}}>

        <View style={{width:'100%',height:'100%',position:'relative'}}>
        <Image style={{width:'100%',height:'100%'}} source={bg_01} />


        <TouchableOpacity onPress={()=>navigation.goBack()} style={{position:'absolute',top:4,left:4}}>
            
            <Text style={{fontSize:32,fontWeight:'bold',color:'white'}}>{`<`}</Text>
        </TouchableOpacity>
        <View style={{position:'absolute',top:200,backgroundColor:'white' ,width:'100%',height:'100%',borderTopLeftRadius:28,borderTopRightRadius:28,paddingHorizontal:12}}>
        <View style={{alignItems:'center',paddingTop:12,paddingBottom:20}}>
            <View style={{backgroundColor:'black',width:40,borderRadius:100,height:4}}/>
        </View>
        {/* <View style={{backgroundColor:'white',flex:1 ,borderRadius:32,padding:4}}> </View> */}
        <View style={{gap:16}}>

        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
            <Text style={{fontSize:24,fontWeight:'bold'}}>Deep Squats</Text>
            <TouchableOpacity><Text >❤️</Text></TouchableOpacity>
        </View>

        <Text>{item.title}</Text>
        
        <View style={{gap:8}}>
        <Text style={{fontSize:20,fontWeight:'bold'}}>BUY THE LOOK</Text>

        <View style={{flexDirection:'row' ,gap:4}}>
        <Image style={{width:150,height:200,borderRadius:16}} source={{uri:item.url}}/>
        <Image style={{width:150,height:200,borderRadius:16}} source={{uri:item.thumbnailUrl}}/>
        
        </View>
        </View>
        
        </View>
        </View>
        </View>

        
 
    </View>
  )
}

export default DetailScreen